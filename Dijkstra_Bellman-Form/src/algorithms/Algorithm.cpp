#include "Algorithm.hpp"
#include <algorithm>
#include <utility>
#include <limits>
Algorithm::Algorithm() {
}

Algorithm::~Algorithm() {
}

void Algorithm::printExecutionTime()
{
	auto duration = std::chrono::duration_cast<std::chrono::milliseconds>(stop-start);
	std::cout<<"Execution time : " << duration.count()<<std::endl;
}

void Algorithm::initializeInitialDistances(const Graph& graph,
		const std::shared_ptr<Node>& sourceNode)
{
	sourceNode->setDistanceToSourceNode(0);
	const auto& nodes = graph.getNodes();

	std::for_each(nodes.begin(),nodes.end(),[&sourceNode](const std::shared_ptr<Node>& node)
	{
		node->setVisited(false);
		if (*sourceNode == *node)
		{
			node->setDistanceToSourceNode(0);
		}
		else
		{
			node->setDistanceToSourceNode(std::numeric_limits<int>::max());
		}
	});
}

/*
 * Algorithm.cpp
 *
 *  Created on: 03.05.2018
 *      Author: �ukasz
 */




