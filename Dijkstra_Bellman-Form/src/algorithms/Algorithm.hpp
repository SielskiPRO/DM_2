#ifndef ALGORITHMS_ALGORITHM_HPP_
#define ALGORITHMS_ALGORITHM_HPP_
#include <memory>
#include <map>
#include <chrono>
#include "Graph.hpp"

using Clock = std::chrono::high_resolution_clock;
using TimePoint = Clock::time_point;

class Algorithm
{
public:
	Algorithm();
	virtual ~Algorithm();
	virtual void findShortestPaths(const Graph& graph,const std::shared_ptr<Node>& sourceNode) = 0;
	void printExecutionTime();
protected:
	void initializeInitialDistances(const Graph& graph,const std::shared_ptr<Node>& sourceNode);
	TimePoint start;
	TimePoint stop;
};

#endif /* ALGORITHMS_ALGORITHM_HPP_ */
