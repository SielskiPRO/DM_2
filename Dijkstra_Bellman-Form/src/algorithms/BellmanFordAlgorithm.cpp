/*
 * BellmanFordAlgorithm.cpp
 *
 *  Created on: 12.05.2018
 *      Author: �ukasz
 */
#include "BellmanFordAlgorithm.hpp"
#include <algorithm>

BellmanFordAlgorithm::BellmanFordAlgorithm()
{

}

void BellmanFordAlgorithm::findShortestPaths(const Graph& graph,const std::shared_ptr<Node>& sourceNode)
{
	if (!graph.isEnstablished())
	{
		std::cout<<"GRAPH WAS NOT ENSTABLISHED! CANNOT RUN ALGORITHM!"<<std::endl;
		return;
	}

	start = Clock::now();
	initializeInitialDistances(graph,sourceNode);

	const auto& links = graph.getLinks();
	for (auto i = 1; i <= int(graph.getNodes().size()-1); i++)
	{
		std::for_each(links.begin(),links.end(),[](const std::shared_ptr<Link>& link)
		{
			auto linkValue = link->getValue();
			const auto& src = link->getSrc();
			const auto& dst = link->getDst();
			auto distanceSrc = src->getDistanceToSourceNode();
			auto distanceDst = dst->getDistanceToSourceNode();
			if (distanceSrc + linkValue < distanceDst)
			{
				dst->setDistanceToSourceNode(distanceSrc+linkValue);
			}
			distanceDst = dst->getDistanceToSourceNode();
			if (distanceDst + linkValue < distanceSrc)
			{
				src->setDistanceToSourceNode(distanceDst+linkValue);
			}
		});
	}
	stop = Clock::now();
}

