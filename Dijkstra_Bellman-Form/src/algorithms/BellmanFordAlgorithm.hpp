/*
 * BellmanFormAlgorithm.hpp
 *
 *  Created on: 24.04.2018
 *      Author: �ukasz
 */

#ifndef ALGORITHMS_BELLMANFORDALGORITHM_HPP_
#define ALGORITHMS_BELLMANFORDALGORITHM_HPP_
#include "Algorithm.hpp"

class BellmanFordAlgorithm : public Algorithm
{
public:
	BellmanFordAlgorithm();
	virtual void findShortestPaths(const Graph& graph,const std::shared_ptr<Node>& sourceNode);
};



#endif /* ALGORITHMS_BELLMANFORDALGORITHM_HPP_ */
