/*
 * DijkstraAlgorithm.cpp
 *
 *  Created on: 04.05.2018
 *      Author: �ukasz
 */
#include "DijkstraAlgorithm.hpp"
#include <algorithm>
#include <set>

DijkstraAlgorithm::DijkstraAlgorithm() {
}

DijkstraAlgorithm::~DijkstraAlgorithm() {
}

void DijkstraAlgorithm::findShortestPaths(const Graph& graph,
		const std::shared_ptr<Node>& sourceNode)
{
	if (!graph.isEnstablished())
	{
		std::cout<<"GRAPH WAS NOT ENSTABLISHED! CANNOT RUN ALGORITHM!"<<std::endl;
		return;
	}
	std::multiset<std::shared_ptr<Node>,GreaterThanByDistance> pq;
	for (const auto& node : graph.getNodes())
	{
		pq.insert(node);
	}
	start = Clock::now();
	initializeInitialDistances(graph,sourceNode);

	while(!pq.empty())
	{
		auto node = *pq.begin();
		pq.erase(pq.begin());
		const auto& neighbourNodes = node->getNeighbourNodes();
		std::for_each(neighbourNodes.begin(),neighbourNodes.end(),[&](const std::shared_ptr<Node>& neighbourNode)
		{
			if (!neighbourNode->isVisited())
			{
				neighbourNode->setVisited(true);
				const auto& link = graph.findLink(node,neighbourNode);
				if (node->getDistanceToSourceNode()+link->getValue() < neighbourNode->getDistanceToSourceNode())
				{
					pq.erase(neighbourNode);
					neighbourNode->setDistanceToSourceNode(node->getDistanceToSourceNode()+link->getValue());
					pq.insert(neighbourNode);
				}
			}
		});
	}
	stop = Clock::now();
}
