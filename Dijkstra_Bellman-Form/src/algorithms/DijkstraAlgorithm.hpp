/*
 * DijkstraAlgorithm.hpp
 *
 *  Created on: 24.04.2018
 *      Author: �ukasz
 */

#ifndef ALGORITHMS_DIJKSTRAALGORITHM_HPP_
#define ALGORITHMS_DIJKSTRAALGORITHM_HPP_
#include "Algorithm.hpp"
#include <queue>

class DijkstraAlgorithm : public Algorithm
{
public:
	DijkstraAlgorithm();
	virtual ~DijkstraAlgorithm();
protected:
	virtual void findShortestPaths(const Graph& graph,const std::shared_ptr<Node>& sourceNode);
private:
};

void print_queue(std::priority_queue<Node,std::vector<Node>,GreaterThanByDistance> q);


#endif /* ALGORITHMS_DIJKSTRAALGORITHM_HPP_ */
