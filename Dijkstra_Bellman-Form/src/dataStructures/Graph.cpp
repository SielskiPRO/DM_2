/*
 * Graph.cpp
 *
 *  Created on: 24.04.2018
 *      Author: �ukasz
 */
#include "Graph.hpp"
#include "RandomGenerator.hpp"
#include <algorithm>
#include <iostream>
#include <ctime>
#include <cstdlib>

const std::shared_ptr<Link>& Graph::findLink(
		const std::shared_ptr<Node>& firstNode,
		const std::shared_ptr<Node>& secondNode) const
{
	auto it = std::find_if(links_.begin(),links_.end(),[&](const std::shared_ptr<Link>& link)
	{
		if ((*link->getDst() == *firstNode && *link->getSrc() == *secondNode) ||
			(*link->getDst() == *secondNode && *link->getSrc() == *firstNode))
		{
			return true;
		}
		else
		{
			return false;
		}
	});
	return *it;
}

Graph::Graph()
{
	enstablished = false;
}

Graph& Graph::getInstance()
{
	static Graph graph;
	return graph;
}

void Graph::addNode(const std::shared_ptr<Node>& node)
{
	if (node)
	{
		nodes_.push_back(node);
	}
}

void Graph::addLink(const std::shared_ptr<Node>& src, const std::shared_ptr<Node>& dest, int cost)
{
	if (src && dest && cost > 0)
	{
		links_.push_back(std::make_shared<Link>(src,dest,cost));
		src->addNeighbourNode(dest);
		dest->addNeighbourNode(src);
	}
	else
	{
		std::cout << "LINK CAN'T BE CREATED!" << std::endl;
	}
}

void Graph::setupNodesAndConnections(int numOfNodes, int maxConnectionsPerNode, int minimalDistance, int maximalDistance)
{
	if (maxConnectionsPerNode < 2 || maxConnectionsPerNode >= numOfNodes)
	{
		std::cout<<"NUMBER OF MAXIMAL CONNECTIONS PER NODE MUST BE GREATER THAN 1 AND SMALLER THAN NUMBER OF NODES!"<<std::endl;
		return;
	}
	resetGraph();
	for (int i = 0; i<numOfNodes; ++i)
	{
		addNode(std::make_shared<Node>());
	}
	Node::MAX_CONNECTIONS = maxConnectionsPerNode;
	createInitialConnections(minimalDistance,maximalDistance);
	std::vector<std::shared_ptr<Node>> sourceNodes(nodes_);

	while (!sourceNodes.empty())
	{
		auto nodeNumberToChoose = RandomGenerator::genIntFromRange(0,sourceNodes.size()-1);
		const auto& choosenNode = *(sourceNodes.begin()+nodeNumberToChoose);

		auto availableNodesToConnect = choosenNode->getAvailableNodesToConnect(nodes_);

		if (!availableNodesToConnect.empty())
		{
			unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
			std::default_random_engine e(seed);
			int howManyConnectionsLeftForChoosenNode = Node::MAX_CONNECTIONS - choosenNode->getNeighbourNodes().size();
			int numberOfConnections;
			if (maxConnectionsPerNode == numOfNodes-1)
			{
				numberOfConnections = availableNodesToConnect.size();
			}
			else
			{
				numberOfConnections = RandomGenerator::genIntFromRange(1,howManyConnectionsLeftForChoosenNode);
			}
			std::shuffle(availableNodesToConnect.begin(),availableNodesToConnect.end(),e);
			std::vector<std::shared_ptr<Node>> choosenNodesToConnect;
			std::copy(availableNodesToConnect.begin(),availableNodesToConnect.begin()+numberOfConnections,std::back_inserter(choosenNodesToConnect));
			for (const auto& nodeToConnect : choosenNodesToConnect)
			{
				addLink(choosenNode,nodeToConnect,RandomGenerator::genIntFromRange(minimalDistance,maximalDistance));
			}
		}
		sourceNodes.erase(sourceNodes.begin()+nodeNumberToChoose);
	}
	enstablished = true;
}

const std::vector<std::shared_ptr<Link> >& Graph::getLinks() const
{
	return links_;
}

const std::vector<std::shared_ptr<Node> >& Graph::getNodes() const
{
	return nodes_;
}

std::ostream& operator<<(std::ostream& out, const Graph& graph)
{
	const auto& nodes = graph.getNodes();
	for (const auto& node : nodes)
	{
		out << "Node number : " << node->getId() << " has " <<node->getNeighbourNodes().size() << " neighbours: \n";
		for (const auto& neighbour : node->getNeighbourNodes())
		{
			out << "Neighbour node : " << neighbour->getId() <<std::endl;
		}
	}
	return out;
}


void Graph::resetGraph() {
	links_.clear();
	nodes_.clear();
	Node::nextId_ = 0;
}

void Graph::createInitialConnections(int minimalDistance, int maximalDistance)
{
	bool areConnected = false;
	auto initialNode = *(nodes_.begin());
	while (!areConnected)
	{
		std::vector<std::shared_ptr<Node>> availableNodes;
		std::copy_if(nodes_.begin(),nodes_.end(),std::back_inserter(availableNodes),[&initialNode](const std::shared_ptr<Node> node)
		{
			if (*node == *initialNode){return false;}
			if (node->getNeighbourNodes().empty()){return true;}
			return false;
		});
		if (availableNodes.empty())
		{
			areConnected = true;
		}
		else
		{
			auto size = availableNodes.size();
			const auto choosenNode = availableNodes.at(RandomGenerator::genIntFromRange(0,size-1));
			addLink(initialNode,choosenNode,RandomGenerator::genIntFromRange(minimalDistance,maximalDistance));
			initialNode = choosenNode;
		}
	}
}

bool Graph::isEnstablished() const {
	return enstablished;
}
