/*
 * Graph.hpp
 *
 *  Created on: 24.04.2018
 *      Author: �ukasz
 */

#ifndef DATASTRUCTURES_GRAPH_HPP_
#define DATASTRUCTURES_GRAPH_HPP_
#include <vector>
#include <memory>
#include "Node.hpp"
#include "Link.hpp"

class Graph
{
public:
	static Graph& getInstance();
	void addNode(const std::shared_ptr<Node>& node);
	void addLink(const std::shared_ptr<Node>& src, const std::shared_ptr<Node>& dest, int cost);
	void setupNodesAndConnections(int numOfNodes, int maxConnectionsPerNode, int minimalDistance, int maximalDistance);
	const std::shared_ptr<Link>& findLink(const std::shared_ptr<Node>& firstNode, const std::shared_ptr<Node>& secondNode) const;
	void resetGraph();
	Graph(const Graph&) = delete;
	Graph(const Graph&&) = delete;
	Graph& operator=(const Graph&) = delete;
	Graph& operator=(const Graph&&) = delete;
	friend std::ostream& operator<<(std::ostream& out, const Graph& graph);

	const std::vector<std::shared_ptr<Link> >& getLinks() const;
	const std::vector<std::shared_ptr<Node> >& getNodes() const;
	bool isEnstablished() const;

private:
	Graph();
	void createInitialConnections(int minimalDistance, int maximalDistance);
	std::vector<std::shared_ptr<Node>> nodes_;
	std::vector<std::shared_ptr<Link>> links_;
	bool enstablished;
};
#endif /* DATASTRUCTURES_GRAPH_HPP_ */
