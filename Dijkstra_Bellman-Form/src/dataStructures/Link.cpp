/*
 * Link.cpp
 *
 *  Created on: 25.04.2018
 *      Author: �ukasz
 */
#include "Link.hpp"

Link::Link(const std::shared_ptr<Node>& src, const std::shared_ptr<Node>& dst, int value)
{
	src_ = src;
	dst_ = dst;
	value_ = value;
}

int Link::getValue() const {
	return value_;
}

void Link::setValue(int value) {
	this->value_ = value;
}

const std::shared_ptr<Node>& Link::getDst() const {
	return dst_;
}

void Link::setDst(const std::shared_ptr<Node>& dst) {
	dst_ = dst;
}

const std::shared_ptr<Node>& Link::getSrc() const {
	return src_;
}

void Link::setSrc(const std::shared_ptr<Node>& src) {
	src_ = src;
}

bool Link::operator ==(const Link& rhs) const {
	return (src_ == rhs.src_ && dst_ == rhs.dst_ && value_ == rhs.value_)
			|| (src_ == rhs.dst_ && dst_ == rhs.src_ && value_ == rhs.value_);
}

std::ostream& operator<<(std::ostream& out, const Link& rhs)
{
	const auto& src = rhs.getSrc();
	const auto& dst = rhs.getDst();
	out << "(" << src->getId() << "," << dst->getId() << ")";
	return out;
}
