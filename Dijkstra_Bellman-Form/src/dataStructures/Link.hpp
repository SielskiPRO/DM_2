/*
 * Link.hpp
 *
 *  Created on: 24.04.2018
 *      Author: �ukasz
 */

#ifndef DATASTRUCTURES_LINK_HPP_
#define DATASTRUCTURES_LINK_HPP_
#include "Node.hpp"

class Link
{
public:
	explicit Link(const std::shared_ptr<Node>& src,const std::shared_ptr<Node>& dst, int value);
	int getValue() const;
	void setValue(int value);
	const std::shared_ptr<Node>& getDst() const;
	void setDst(const std::shared_ptr<Node>& dst);
	const std::shared_ptr<Node>& getSrc() const;
	void setSrc(const std::shared_ptr<Node>& src);
	bool operator==(const Link& rhs) const;
	friend std::ostream& operator<<(std::ostream& out,const Link& rhs);
private:
	std::shared_ptr<Node> src_;
	std::shared_ptr<Node> dst_;
	int value_;
};
#endif /* DATASTRUCTURES_LINK_HPP_ */
