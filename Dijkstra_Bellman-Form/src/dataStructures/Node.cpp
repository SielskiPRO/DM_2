#include "Node.hpp"
#include <string>
#include <algorithm>
#include <limits>

int Node::nextId_ = 0;
int Node::MAX_CONNECTIONS = 0;

Node::Node() {
	id_ = ++nextId_;
	distanceToSourceNode_ = std::numeric_limits<int>::max();
	visited = false;
}

int Node::getId() const {
	return id_;
}

const std::vector<std::shared_ptr<Node> >& Node::getNeighbourNodes() const {
	return neighbourNodes_;
}

void Node::addNeighbourNode(const std::shared_ptr<Node>& node) {
	if (node)
	{
		neighbourNodes_.push_back(node);
	}
}

bool Node::operator ==(const Node& rhs) {
	if (this->id_ == rhs.id_ && this->neighbourNodes_ == rhs.neighbourNodes_)
	{
		return true;
	}
	return false;
}

Node::~Node() {
}

std::ostream& operator <<(std::ostream& out, const Node& node){
	out << "Node : " << node.getId() << " -> Distance from source node : " << node.getDistanceToSourceNode() << std::endl;
	return out;
}

int Node::getDistanceToSourceNode() const {
	return distanceToSourceNode_;
}

void Node::setDistanceToSourceNode(int distanceToSourceNode) {
	this->distanceToSourceNode_ = distanceToSourceNode;
}

bool GreaterThanByDistance::operator ()(const std::shared_ptr<Node>& lhs,
		const std::shared_ptr<Node>& rhs)
{
	return lhs->getDistanceToSourceNode() < rhs->getDistanceToSourceNode();
}

std::vector<std::shared_ptr<Node> > Node::getAvailableNodesToConnect(const std::vector<std::shared_ptr<Node>>& allNodes) const
{
	std::vector<std::shared_ptr<Node>> availableNodes;
	if(this->neighbourNodes_.size() == Node::MAX_CONNECTIONS){return {};}
	std::copy_if(allNodes.begin(),allNodes.end(),std::back_inserter(availableNodes),[&](const std::shared_ptr<Node>& node)
	{
		if (*node == *this){return false;}
		if (node->getNeighbourNodes().size() == Node::MAX_CONNECTIONS){return false;}
		auto isNeighbour = std::find(neighbourNodes_.begin(),neighbourNodes_.end(),node);
		if (isNeighbour != neighbourNodes_.end()){return false;}
		return true;
	});
	return availableNodes;
}

bool Node::isVisited() const {
	return visited;
}

void Node::setVisited(bool visited) {
	this->visited = visited;
}
