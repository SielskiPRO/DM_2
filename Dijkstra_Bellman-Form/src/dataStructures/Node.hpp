/*
 * Node.hpp
 *
 *  Created on: 24.04.2018
 *      Author: �ukasz
 */

#ifndef DATASTRUCTURES_NODE_HPP_
#define DATASTRUCTURES_NODE_HPP_
#include <vector>
#include <memory>
#include <iostream>

class Node
{
public:
	Node();
	~Node();
	int getId() const;
	const std::vector<std::shared_ptr<Node>>& getNeighbourNodes() const;
	void addNeighbourNode(const std::shared_ptr<Node>& node);
	bool operator==(const Node& rhs);
	int getDistanceToSourceNode() const;
	friend std::ostream& operator<<(std::ostream& out, const Node& node);
	void setDistanceToSourceNode(int distanceToSourceNode);
    std::vector<std::shared_ptr<Node>> getAvailableNodesToConnect(const std::vector<std::shared_ptr<Node>>& allNodes) const;
	bool isVisited() const;
	void setVisited(bool visited);

	static int MAX_CONNECTIONS;
	static int nextId_;
private:
	std::vector<std::shared_ptr<Node>> neighbourNodes_;
	int id_;
	int distanceToSourceNode_;
	bool visited;
};

struct GreaterThanByDistance
{
	bool operator()(const std::shared_ptr<Node>& lhs, const std::shared_ptr<Node>& rhs);
};
#endif /* DATASTRUCTURES_NODE_HPP_ */
