//============================================================================
// Name        : Dijkstra_Bellman-Form.cpp
// Author      : 
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <gtest/gtest.h>
#include <GraphShould.hpp>
#include "Graph.hpp"
#include "DijkstraAlgorithm.hpp"
#include "BellmanFordAlgorithm.hpp"
//#define RUN_TESTS

using namespace std;

int main(int argc, char** argv) {
#ifdef RUN_TESTS
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
#endif
	Graph& graph = Graph::getInstance();
    graph.setupNodesAndConnections(1000,999,1,1);
    std::unique_ptr<Algorithm> ptr = std::make_unique<DijkstraAlgorithm>();
    if (graph.isEnstablished())
    {
    	std::cout<<"GRAPH : "<<std::endl;
    	std::cout<<graph<<std::endl;
    	std::cout<<"DIJKSTRA Algorithm : "<<std::endl;
    	ptr->findShortestPaths(graph,graph.getNodes().at(0));
    	for (const auto& node : graph.getNodes())
    	{
    	    cout<<*node<<endl;
    	}
    	ptr->printExecutionTime();
    	ptr = std::make_unique<BellmanFordAlgorithm>();
    	std::cout<<"BELLMAN-FORD Algorithm : "<<std::endl;
    	ptr->findShortestPaths(graph,graph.getNodes().at(0));
    	for (const auto& node : graph.getNodes())
    	{
    		cout<<*node<<endl;
    	}
    	ptr->printExecutionTime();
    }
    return 0;
}
