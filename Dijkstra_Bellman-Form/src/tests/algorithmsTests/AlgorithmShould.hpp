/*
 * AlgorithmTests.hpp
 *
 *  Created on: 04.05.2018
 *      Author: �ukasz
 */

#ifndef TESTS_ALGORITHMSTESTS_ALGORITHMSHOULD_HPP_
#define TESTS_ALGORITHMSTESTS_ALGORITHMSHOULD_HPP_
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <memory>
#include "Node.hpp"
#include "Link.hpp"

using namespace ::testing;

const int NUM_OF_NODES = 5;

class AlgorithmShould : public ::testing::Test
{
public:
	AlgorithmShould()
	{
		for (auto i = 0; i<NUM_OF_NODES; ++i)
			graph_.addNode(std::make_shared<Node>());
	}
	virtual AlgorithmShould(){};
protected:
	Graph& graph_ = Graph::getInstance();

};

#endif /* TESTS_ALGORITHMSTESTS_ALGORITHMSHOULD_HPP_ */
