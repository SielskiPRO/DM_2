/*
 * GraphTestShould.h
 *
 *  Created on: 29.04.2018
 *      Author: �ukasz
 */

#ifndef TESTS_GRAPHTESTS_GRAPHSHOULD_HPP_
#define TESTS_GRAPHTESTS_GRAPHSHOULD_HPP_
#include <gtest/gtest.h>
#include <gmock/gmock.h>
#include <iostream>
#include "Graph.hpp"
#include "Node.hpp"
#include "Link.hpp"
#include "matchers.hpp"

using namespace ::testing;

class GraphShould : public ::testing::Test
{
public:
	GraphShould(){}
	virtual ~GraphShould(){}
protected:
	Graph& graph_ = Graph::getInstance();
	std::shared_ptr<Node> initializedSrc = std::make_shared<Node>();
	std::shared_ptr<Node> initializedDest = std::make_shared<Node>();
	std::shared_ptr<Node> uninitializedSrc = nullptr;
	std::shared_ptr<Node> uninitializedDest = nullptr;
	int allowedCost = 5;
	int forbiddenCost = -1;

};

TEST_F(GraphShould,addLink)
{
	graph_.resetGraph();
	graph_.addLink(initializedSrc,initializedDest,allowedCost);
	EXPECT_THAT(graph_.getLinks(),SizeIs(1));
}

TEST_F(GraphShould,dontAddLinkIfSrcNodeNotInitialized)
{
	graph_.resetGraph();
	graph_.addLink(uninitializedSrc,initializedDest,allowedCost);
	EXPECT_THAT(graph_.getLinks(),SizeIs(0));
}

TEST_F(GraphShould,dontAddLinkIfDestNodeNotInitialized)
{
	graph_.resetGraph();
	graph_.addLink(initializedSrc,uninitializedDest,allowedCost);
	EXPECT_THAT(graph_.getLinks(),SizeIs(0));
}

TEST_F(GraphShould,dontAddLinkIfCostHasForbiddenValue)
{
	graph_.resetGraph();
	graph_.addLink(initializedSrc,initializedDest,forbiddenCost);
	EXPECT_THAT(graph_.getLinks(),SizeIs(0));
}

TEST_F(GraphShould,containsProperAmountOfNodes)
{
	graph_.resetGraph();
	graph_.setupNodesAndConnections(5,2,1,100);
	EXPECT_EQ(graph_.getNodes().size(),5u);
}

#endif /* TESTS_GRAPHTESTS_GRAPHSHOULD_HPP_ */
