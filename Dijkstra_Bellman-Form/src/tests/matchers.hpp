/*
 * matchers.hpp
 *
 *  Created on: 29.04.2018
 *      Author: �ukasz
 */

#ifndef TESTS_MATCHERS_HPP_
#define TESTS_MATCHERS_HPP_
#include <gmock/gmock.h>
#include <algorithm>

MATCHER_P(DoesNotContain,node,"")
{
	if (std::find(arg.begin(),arg.end(),node) == arg.end())
	{
		return true;
	}
	return false;
};



#endif /* TESTS_MATCHERS_HPP_ */
