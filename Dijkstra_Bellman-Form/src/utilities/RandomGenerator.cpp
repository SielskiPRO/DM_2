/*
 * RandomGenerator.cpp
 *
 *  Created on: 30.04.2018
 *      Author: �ukasz
 */

#include "RandomGenerator.hpp"

std::mt19937 RandomGenerator::gen {std::chrono::high_resolution_clock::now().time_since_epoch().count()};

int RandomGenerator::genIntFromRange(int lowerBoundary, int upperBoundary)
{
	std::uniform_int_distribution<> dis{lowerBoundary,upperBoundary};
	return dis(gen);
}
