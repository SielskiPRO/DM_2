/*
 * RandomGenerator.h
 *
 *  Created on: 30.04.2018
 *      Author: �ukasz
 */

#ifndef UTILITIES_RANDOMGENERATOR_HPP_
#define UTILITIES_RANDOMGENERATOR_HPP_
#include <random>
#include <chrono>

typedef std::chrono::high_resolution_clock myclock;

class RandomGenerator {

public:
	static int genIntFromRange(int lowerBoundary, int upperBoundary);
private:
	static std::random_device rd;
	static std::mt19937 gen;
};

#endif /* UTILITIES_RANDOMGENERATOR_HPP_ */
